<?php

namespace Hofstaetter\Feiertage;

use Carbon\Carbon;
use Exception;

class FeiertagIndex
{
    /**
     * @var string[] Fix dates to check against, formatted m-d
     */
    public static array $fixDates = [
        '01-01' => 'Neujahr',
        '01-06' => 'Heilige Drei Könige',
        '05-01' => 'Staatsfeiertag',
        '08-15' => 'Mariä Himmelfahrt',
        '10-26' => 'Nationalfeiertag',
        '11-01' => 'Allerheiligen',
        '11-02' => 'Allerseelen',
        '12-08' => 'Mariä Empfängnis',
        '12-25' => 'Weihnachten',
        '12-26' => 'Stephanstag',
    ];

    /**
     * @var string[] dynamic holiday rules, key is name and value is rule definition.
     * Rules need to have a name and a single parameter separated by a colon.
     * When the rule is checked a static method of the class with the rule name is invoked with the given day
     * and the given rule parameter.
     */
    public static array $rules = [
        'Ostermontag' => 'easter:+1',
        'Christi Himmelfahrt' => 'easter:+39',
        'Pfingstmontag' => 'easter:+50',
        'Fronleichnam' => 'easter:+60',
    ];

    /**
     * Checks if the given date is an austrian national holiday.
     * This does not cover regional holidays.
     *
     * @param Carbon $day The day to check
     * @return string|false Returns the name of the holiday or false if the day is not a holiday
     * @throws Exception if there is no static method matching the rule name
     */
    public static function isFeiertag(Carbon $day): string|false {
        $monthDay = $day->format('m-d');
        if (array_key_exists($monthDay, static::$fixDates)) {
            return static::$fixDates[$monthDay];
        }

        foreach (static::$rules as $name => $rule) {
            [ $ruleName, $ruleParameter ] = explode(':', $rule, 2);

            if (!method_exists(static::class, $ruleName))
                throw new Exception("Rule $ruleName has no check function!");

            if (static::$ruleName($day, $ruleParameter))
                return $name;
        }
        return false;
    }

    /**
     * Checks a day against an easter rule
     * @param Carbon $day the day to check
     * @param string $offset offset in days after Easter
     * @return bool true if the given day matches the rule
     */
    protected static function easter(Carbon $day, string $offset): bool {
        $easterDate = Carbon::create($day->year, 3, 21)
            ->addDays(easter_days($day->year))
            ->addDays((int) $offset);

        return $easterDate->isSameDay($day);
    }
}